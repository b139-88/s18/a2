let courses = [

	{
		id: "1",
		name: "Python 101",
		description: "Learn the basics of python.",
		price: 15000,
		isActive: true

	},
	{
		id: "2",
		name: "CSS 101",
		description: "Learn the basics of CSS.",
		price: 10500,
		isActive: true

	},
	{
		id: "3",
		name: "CSS 102",
		description: "Learn an advanced CSS.",
		price: 15000,
		isActive: false

	},
	{
		id: "4",
		name: "PHP-Laravel 101",
		description: "Learn the basics of PHP and its Laravel framework.",
		price: 20000,
		isActive: true

	}
];

function Course(id, name, description, price, isActive){
    this.id = id;
    this.name = name;
    this.description = description;
    this.price = price;
    this.isActive = isActive;
}

addCourse = (id, name, description, price, isActive) => {
    let NewCourse = {
        id: id,
		name: name,
		description: description,
		price: price,
		isActive: isActive
    };

    courses.push(NewCourse);
    console.log(`You have created ${name}. Its price is ${price}`);
};


addCourse("5", "Unit Testing", "Learn Unit Testing", "15000", true);

getSingleCourse = (id) => {
    let foundCourse = courses.find(course => course.id == id);

    console.log(foundCourse);
};

console.log(`Get single course`);
getSingleCourse("3");

getAllCourses = () => {
    courses.forEach((course) => {
        console.log(course);
    });
};

console.log(`Get all courses`);
getAllCourses();

archiveCourse = (name) => {
    courses.find(course => {
        if (course.name == name) {
            course.isActive = false;
            console.log(`Successfully archived ${course.name}`);
        }
    });
};

archiveCourse("Python 101");

deleteCourse = () => {
    console.log(`Successfully deleted ${courses[courses.length - 1].name}`);
    courses.pop(courses.length - 1);
};

console.log(`Delete course`);
deleteCourse();

showActiveCourses = () => {
    let activeCourses = courses.filter((course) => course.isActive);

    console.log(activeCourses);
};

console.log(`Show active courses`);
showActiveCourses();




